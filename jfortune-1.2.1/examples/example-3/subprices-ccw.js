var options = {
  prices: [
    {
      name: 'Růžová'
    },
    {
      name: 'Červená'
    },
    {
      name: 'Žlutá'
    },
    {
      name: 'Zelená'
    },
    {
      name: 'Modrá'
    },
    {
      name: 'Fialová'
    },
  ],
  duration: 2000,
  clockWise: false
};

var $r = $('.roulette').fortune(options);

var clickHandler = function() {
  $('.spinner').off('click');
  $('.spinner span').hide();
  var price = Math.floor((Math.random() * 6));
  $r.spin(price).done(function(price) {
      $('.price').text('Cena: ' + price.name);
      $('.spinner').on('click', clickHandler);
      $('.spinner span').show();
    });
};

$('.spinner').on('click', clickHandler);
